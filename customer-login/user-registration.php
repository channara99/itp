<?php

use Phppot\Member;

if (!empty($_POST["signup-btn"])) {
	require_once './Model/Member.php';
	$member = new Member();
	$registrationResponse = $member->registerMember();
}
?>
<HTML>

<HEAD>
	<TITLE>User Registration</TITLE>
	<link href="assets/css/phppot-style.css" type="text/css" rel="stylesheet" />
	<link href="assets/css/user-registration.css" type="text/css" rel="stylesheet" />
	<script src="vendor/jquery/jquery-3.3.1.js" type="text/javascript"></script>

	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css" rel="stylesheet" />
</HEAD>

<body>
	<div class="container-fluid p-0">
		<div class="">
			<div class="container bg-white w-50 rounded p-4 my-5">
				<div class="container w-75">
					<ul class="nav nav-pills nav-fill mb-3" id="ex1" role="tablist">
						<li class="nav-item" role="presentation">
							<a class="nav-link active" href="http://localhost/itp/index1.php" aria-selected="true">Home</a>
						</li>
						<li class="nav-item" role="presentation">
							<a class="nav-link" href="index.php" aria-selected="false">Login</a>
						</li>
					</ul>
				</div>
				<div class="container mb-4">
					<form name="sign-up" action="" method="post" onsubmit="return signupValidation()">
						<div class="">
							<p class="text-uppercase text-center" style="font-size: 24px;font-weight: 700;">
								Sign Up
							</p>
						</div>
						<?php
						if (!empty($registrationResponse["status"])) {
						?>
							<?php
							if ($registrationResponse["status"] == "error") {
							?>
								<div class="server-response error-msg"><?php echo $registrationResponse["message"]; ?></div>
							<?php
							} else if ($registrationResponse["status"] == "success") {
							?>
								<div class="server-response success-msg"><?php echo $registrationResponse["message"]; ?></div>
							<?php
							}
							?>
						<?php
						}
						?>
						<div class="container w-75">
							<div class="error-msg" id="error-msg"></div>
							<div class="mb-2">
								<div class="form-label">
									Username<span class="required error" id="username-info"></span>
								</div>
								<input class="form-control" type="text" name="username" id="username" required>
							</div>

							<div class="mb-2">

								<div class="form-label">
									Email<span class="required error" id="email-info"></span>
								</div>
								<input class="form-control" type="email" name="email" id="email" required>
							</div>

							<div class="mb-2">
								<div class="form-label">
									Password<span class="required error" id="signup-password-info"></span>
								</div>
								<input class="form-control" type="password" name="signup-password" id="signup-password" required>
							</div>

							<div class="mb-4">
								<div class="form-label">
									Confirm Password<span class="required error" id="confirm-password-info"></span>
								</div>
								<input class="form-control" type="password" name="confirm-password" id="confirm-password" required>
							</div>
							<div class="mb-4">
								<input class="btn btn-primary w-100" type="submit" name="signup-btn" id="signup-btn" value="Sign up">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script>
		function signupValidation() {
			var valid = true;

			$("#username").removeClass("error-field");
			$("#email").removeClass("error-field");
			$("#password").removeClass("error-field");
			$("#confirm-password").removeClass("error-field");

			var UserName = $("#username").val();
			var email = $("#email").val();
			var Password = $('#signup-password').val();
			var ConfirmPassword = $('#confirm-password').val();
			var emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

			$("#username-info").html("").hide();
			$("#email-info").html("").hide();

			if (UserName.trim() == "") {
				$("#username-info").html("required.").css("color", "#ee0000").show();
				$("#username").addClass("error-field");
				valid = false;
			}
			if (email == "") {
				$("#email-info").html("required").css("color", "#ee0000").show();
				$("#email").addClass("error-field");
				valid = false;
			} else if (email.trim() == "") {
				$("#email-info").html("Invalid email address.").css("color", "#ee0000").show();
				$("#email").addClass("error-field");
				valid = false;
			} else if (!emailRegex.test(email)) {
				$("#email-info").html("Invalid email address.").css("color", "#ee0000")
					.show();
				$("#email").addClass("error-field");
				valid = false;
			}
			if (Password.trim() == "") {
				$("#signup-password-info").html("required.").css("color", "#ee0000").show();
				$("#signup-password").addClass("error-field");
				valid = false;
			}
			if (ConfirmPassword.trim() == "") {
				$("#confirm-password-info").html("required.").css("color", "#ee0000").show();
				$("#confirm-password").addClass("error-field");
				valid = false;
			}
			if (Password != ConfirmPassword) {
				$("#error-msg").html("Both passwords must be same.").show();
				valid = false;
			}
			if (valid == false) {
				$('.error-field').first().focus();
				valid = false;
			}
			return valid;
		}
	</script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.js"></script>
</body>

</HTML>