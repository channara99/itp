<?php

use Phppot\Member;

if (!empty($_POST["login-btn"])) {
	require_once __DIR__ . '/Model/Member.php';
	$member = new Member();
	$loginResult = $member->loginMember();
}
?>
<HTML>

<HEAD>
	<TITLE>Login</TITLE>
	<link href="assets/css/phppot-style.css" type="text/css" rel="stylesheet" />
	<link href="assets/css/user-registration.css" type="text/css" rel="stylesheet" />
	<script src="vendor/jquery/jquery-3.3.1.js" type="text/javascript"></script>

	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css" rel="stylesheet" />

</HEAD>

<body class="">
	<div class="container-fluid p-0">
		<div class="">
			<div class="container bg-white w-50 rounded p-4 my-5">
				<div class="container w-75">
					<!-- <a href="http://localhost/itp/index1.php">Home</a>
					<a href="user-registration.php" class="mx-5">Sign up</a> -->
					<ul class="nav nav-pills nav-fill mb-3" id="ex1" role="tablist">
						<li class="nav-item" role="presentation">
							<a class="nav-link active" href="http://localhost/itp/index1.php" aria-selected="true">Home</a>
						</li>
						<li class="nav-item" role="presentation">
							<a class="nav-link" href="user-registration.php" aria-selected="false">Sign Up</a>
						</li>

					</ul>
				</div>
				<div class="container my-3 mb-5">
					<form name="login" action="" method="post" onsubmit="return loginValidation()">
						<div class="">
							<p class="text-uppercase text-center" style="font-size: 24px;font-weight: 700;">
								Login
							</p>
						</div>
						<?php if (!empty($loginResult)) { ?>
							<div class="error-msg"><?php echo $loginResult; ?></div>
						<?php } ?>
						<div class="container w-75">
							<div class="mb-2">
								<div class="form-label">
									Username<span class="required error" id="username-info"></span>
								</div>
								<input class="form-control" type="text" name="username" id="username" required>
							</div>
							<div class="mb-4">
								<div class="form-label">
									Password<span class="required error" id="login-password-info"></span>
								</div>
								<input class="form-control" type="password" name="login-password" id="login-password" required>
							</div>
							<div class="mb-3">
								<input class="btn btn-primary w-100" type="submit" name="login-btn" id="login-btn" value="Login">
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
	<script>
		function loginValidation() {
			var valid = true;
			$("#username").removeClass("error-field");
			$("#password").removeClass("error-field");

			var UserName = $("#username").val();
			var Password = $('#login-password').val();

			$("#username-info").html("").hide();

			if (UserName.trim() == "") {
				$("#username-info").html("required.").css("color", "#ee0000").show();
				$("#username").addClass("error-field");
				valid = false;
			}
			if (Password.trim() == "") {
				$("#login-password-info").html("required.").css("color", "#ee0000").show();
				$("#login-password").addClass("error-field");
				valid = false;
			}
			if (valid == false) {
				$('.error-field').first().focus();
				valid = false;
			}
			return valid;
		}
	</script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.js"></script>

</body>

</HTML>