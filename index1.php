<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta charset="utf-8">

    <title>Foody</title>

    <!-- Font-awesome -->
    <link rel="stylesheet" href="css/font-awesome.css" type="text/css" />

    <!-- Main stylesheet -->
    <link rel="stylesheet" href="css/style.css" type="text/css" />



    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lobster|Poppins:40,0,700" rel="stylesheet" />

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/fav.png" />

    <!--Contact-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Working Contact Form HTML/PHP " />
    <!--web-fonts-->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>
<link href='//fonts.googleapis.com/css?family=Candal' rel='stylesheet' type='text/css'>
<!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous"> -->

<!-- Font Awesome -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
<!-- MDB -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<style>
    * {
        box-sizing: border-box;
    }

    .footer-inner {
        margin-top: 40px;
    }

    .form_box {
        width: 400px;
        margin-left: 40%;
        padding: 10px;
        background-color: white;
    }

    input {
        padding: 5px;
        margin-bottom: 5px;
    }

    input[type="submit"] {
        border: none;
        outlin: none;
        background-color: #679f1b;
        color: white;
    }

    .heading {
        background-color: #ac1219;
        color: white;
        height: 40px;
        width: 100%;
        line-height: 40px;
        text-align: center;
    }

    .shadow {
        -webkit-box-shadow: 0px 0px 17px 1px rgba(0, 0, 0, 0.43);
        -moz-box-shadow: 0px 0px 17px 1px rgba(0, 0, 0, 0.43);
        box-shadow: 0px 0px 17px 1px rgba(0, 0, 0, 0.43);
    }

    .pic {
        text-align: left;
        width: 33%;
        float: left;
    }
</style>

</head>

<body>
    <div class="pb-4">
        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
            <!-- Container wrapper -->
            <div class="container-fluid">
                <!-- Toggle button -->
                <button class="navbar-toggler" type="button" data-mdb-toggle="collapse" data-mdb-target="#navbarCenteredExample" aria-controls="navbarCenteredExample" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars"></i>
                </button>

                <!-- Collapsible wrapper -->
                <div class="collapse navbar-collapse justify-content-center my-2" id="navbarCenteredExample">
                    <!-- Left links -->
                    <ul class="navbar-nav mb-2 mb-lg-0 h4">
                        <li class="nav-item px-5">
                            <a class="nav-link active" aria-current="page" href="#">Home</a>
                        </li>
                        <li class="nav-item px-5">
                            <a class="nav-link" href="it project/aboutus.html">About</a>
                        </li>
                        <li class="nav-item px-5">
                            <a class="nav-link" href="shoppingcart2/">Products</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-mdb-toggle="dropdown" aria-expanded="false">
                                Log in
                            </a>
                            <!-- Dropdown menu -->
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li>
                                    <a class="dropdown-item" href="admin/login.php">Admin</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="customer-login/home.php">Customer</a>
                                </li>

                                <li>
                                    <a class="dropdown-item" href="http://localhost/itp/pro/admin/login.php">Vendor</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                    <!-- Left links -->
                </div>
                <!-- Collapsible wrapper -->
            </div>
            <!-- Container wrapper -->
        </nav>
    </div>
    <div class="p-4 mt-1">
        <!-- Class for hide size -->
    </div>
    <div class="">
        <div id="carouselExampleInterval" class="carousel slide shadow-sm" data-mdb-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active" data-mdb-interval="10000">
                    <div class="d-flex">
                        <img src="img/img-1.jpg" class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" width="100%" height="50%" />
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="d-flex">
                        <img src="img/img-2.jpg" class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" width="100%" height="10%" />
                    </div>
                </div>
                <div class="carousel-item" data-mdb-interval="2000">
                    <div class="d-flex">
                        <img src="img/img-3.jpg" class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" width="100%" height="10%" />
                    </div>
                </div>

            </div>
            <button class="carousel-control-prev" data-mdb-target="#carouselExampleInterval" type="button" data-mdb-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" data-mdb-target="#carouselExampleInterval" type="button" data-mdb-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div>
    <div class="container-fluid p-0">
        <div class="">
            <div class="bg-primary" style="background-color: #2390D8;">
                <div class="">
                    <p class="text-center p-4 text-white h4 text-uppercase" style="font-weight: 700;">
                        Khmer Grocery
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid p-0 embed-responsive">
        <div class="my-5">
            <div class="container card rounded w-50" role="group" aria-label="Basic example">
                <div class="bg-white px-5 pt-2 pb-5">
                    <div class="text-center mt-2">
                        <p class="h5" style="font-weight: 900;">
                            សូមធ្វើការទាក់ទង
                        </p>
                    </div>
                    <div class="">
                        <form action="send_form_email.php" method="post" class="">
                            <div class="my-2">
                                <label for="">ឈ្មោះអតិថិជន</label>
                                <div class="form-outline mt-2">
                                    <input type="text" name="name" class="form-control" required />
                                    <label class="form-label" for="form1" class="text-sm" style="font-size: 12px;">បញ្ចូលឈ្មោះរបស់អ្នក</label>
                                </div>
                            </div>
                            <div class="my-2">
                                <label for="">អ៊ីម៉ែល</label>
                                <div class="form-outline mt-2">
                                    <input type="text" id="form1" class="form-control" name="email" required />
                                    <label class="form-label" for="form1" class="text-sm" style="font-size: 12px;">អ៊ីម៉ែល</label>

                                </div>
                            </div>
                            <div class="my-2">
                                <label for="">លេខទូរស័ព្ទ</label>
                                <div class="form-outline mt-2">
                                    <input type="text" id="form1" class="form-control" name="phone" required />
                                    <label class="form-label" for="form1" class="text-sm" style="font-size: 12px;">លេខទូរស័ព្ទ</label>

                                </div>
                            </div>
                            <div class="my-2">
                                <label for="">គោលបំណង</label>
                                <div class="form-outline mt-2">
                                    <input type="text" id="form1" class="form-control" name="subject" required />
                                    <label class="form-label" for="form1" class="text-sm" style="font-size: 12px;">គោលបំណង</label>

                                </div>
                            </div>
                            <div class="my-2">
                                <label for="">សារបន្ថែម</label>
                                <div class="form-outline mt-2">
                                    <textarea class="form-control" id="textAreaExample" rows="5" name="message"></textarea>
                                    <label class="form-label" for="form1" class="text-sm" style="font-size: 12px;">សារបន្ថែម</label>

                                </div>
                            </div>
                            <div class="my-3">
                                <div class="">
                                    <!-- <input type="submit" value="ដាក់ផ្ញើរ" class="btn btn-primary btn-md"> -->
                                    <button type="button" class="btn btn-success btn-md">ដាក់ផ្ញើរ</button>

                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <section class="mt-5 footer-inner">
        <footer class="text-center text-white" style="background-color: #006ACE; margin-top: 100px;">
            <div class="container p-4 pb-0">
                <section class="">
                    <p class="d-flex justify-content-center align-items-center">
                        <span class="me-3">ចូលបង្កើតគណនី</span>
                        <a href="customer-login/home.php" class="btn btn-outline-light btn-rounded">Sign Up!</a>
                    </p>
                </section>
            </div>
            <div class="text-center p-3" style="background-color: #0A63B6;">
                © 2021 : រក្សាសិទ្ទិដោយ៖
                <!-- <a class="text-white text-uppercase" href="#" disabled>Khmer Grocery</a> -->
                <span class="text-white text-uppercase">
                    Khmer Grocery
                </span>
            </div>
        </footer>
    </section>
</body>
<!-- MDB -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.js"></script>


</html>